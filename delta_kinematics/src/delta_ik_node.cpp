#include <delta_kinematics/delta_ik.h>
#include <string>
#include <ros/console.h>
#include <ros/ros.h>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/JointState.h>
#include <cmath>

using namespace delta_kinematics;

class IKNode
{
    public:
        ros::NodeHandle nh;
        IKNode()
        {
            nh = ros::NodeHandle("lemon_delta_kinematics");
            sub = nh.subscribe<geometry_msgs::Point>("/lemon_delta_kinematics/desired_cartesian", 5, &IKNode::cb_cartesian, this);
            pub = nh.advertise<sensor_msgs::JointState>("/lemon_delta_driver/servo_cmd", 5);
            // Measures were taken by visual inspection ;)
            // ToDo: URDF parsing to build the robot :O
            double cos60 =  0.5;
            double sin60 = 0.866025404;
            DeltaLeg lemon_leg;
            lemon_leg.a_x = -sin60;
            lemon_leg.a_y = cos60;
            lemon_leg.a_z = 0.0;
            lemon_leg.u0_x = cos60;
            lemon_leg.u0_y = sin60;
            lemon_leg.u0_z = 0.0;
            lemon_leg.b_x = 0.13*cos60;
            lemon_leg.b_y = 0.13*sin60;
            lemon_leg.b_z = 0.0;
            lemon_leg.p_x = 0.04*cos60;
            lemon_leg.p_y = 0.04*sin60;
            lemon_leg.p_z = 0.0;
            lemon_leg.l_b = 0.13;
            lemon_leg.l_p = 0.16;
            // lemon_leg.angle_min = -M_PI/19.0;
            // lemon_leg.angle_max = M_PI/6.0;
            lemon_leg.angle_min = 0;
            lemon_leg.angle_max = 2*M_PI/3;
            createDeltaRobot(lemon_leg, lemon);
            lemon_ik = DeltaIKSolver(lemon);
        }
    private:
        ros::Subscriber sub;
        ros::Publisher pub;

        DeltaRobot lemon;
        DeltaIKSolver lemon_ik;

        void
        cb_cartesian(const geometry_msgs::Point::ConstPtr &msg)
        {
            double cart_x, cart_y, cart_z;
            cart_x = msg->x;
            cart_y = msg->y;
            cart_z = msg->z;
            double A_ang, B_ang, C_ang;
            if(lemon_ik.CartToJnt(cart_x, cart_y, cart_z, A_ang, B_ang, C_ang)){
                //found a solution
                sensor_msgs::JointState jnt_msg;
                jnt_msg.header.stamp = ros::Time::now();
                jnt_msg.header.frame_id = "delta_box";
                jnt_msg.name.push_back("A_first_joint");
                jnt_msg.name.push_back("B_first_joint");
                jnt_msg.name.push_back("C_first_joint");
                jnt_msg.position.push_back(A_ang);
                jnt_msg.position.push_back(B_ang);
                jnt_msg.position.push_back(C_ang);
                pub.publish(jnt_msg);
            }
            else
                ROS_ERROR("No inverse kinematics solution found");
        }

};
int main(int argc, char **argv)
{
    ros::init(argc, argv, "lemon_delta_kinematics");

    IKNode node;
    ros::Rate rate(50);
    while (ros::ok())
    {
        ros::spinOnce();
        rate.sleep();
    }
    return 0;
}
