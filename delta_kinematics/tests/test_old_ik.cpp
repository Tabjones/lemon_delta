//#include <control_msgs/JointControllerState.h> // TODO: state message for all controllers?

#include <urdf/model.h>
//#include <hardware_interface/joint_command_interface.h>
//#include <controller_interface/controller.h>
#include <ros/node_handle.h>
#include <ros/ros.h>



#include <kdl/tree.hpp>
#include <kdl/kdl.hpp>
#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/frames.hpp>
#include <kdl/chaindynparam.hpp> //this to compute the gravity vector
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl_parser/kdl_parser.hpp>

#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl/chainiksolver.hpp>
#include <kdl/chainiksolverpos_nr.hpp>

#include <vector>

// #include <iostream>
// #include <stdio.h>
// #include <unistd.h>
// #include <fcntl.h>
// #include <termios.h>
// #include <string.h> // needed for memset
// #include <stdint.h>
// #include <time.h>
// #include <eigen3/Eigen/Eigen>
// #include <unistd.h>
// #include <fstream>
// #include <SerialStream.h> // libSerial
// #include <math.h>
// #include <ctime>
// #include <cstdlib>
// #include <cstring>
// #include <complex>
// #include <signal.h>
// #include <eigen3/Eigen/SVD>
// #include <sys/time.h>

// #include <stddef.h>
// #include <stdint.h>

// using namespace Eigen;
// using namespace std;

// static uint8_t s_cExtrReachMask;

// //GLOBAL VARIABLES
//         float pi = 3.1415;
//         int tty_fd;
//         char *porta =(char*)"/dev/ttyACM0"; 

        
// //Arduino FUNCTION
// void init_Arduino(){
//         struct termios tio;
//         struct termios stdio;
//         fd_set rdset;      
        
//         memset(&stdio,0,sizeof(stdio));
//         stdio.c_iflag=0;
//         stdio.c_oflag=0;
//         stdio.c_cflag=0;
//         stdio.c_lflag=0;
//         stdio.c_cc[VMIN]=1;
//         stdio.c_cc[VTIME]=0;
//         tcsetattr(STDOUT_FILENO,TCSANOW,&stdio);
//         tcsetattr(STDOUT_FILENO,TCSAFLUSH,&stdio);
//         fcntl(STDIN_FILENO, F_SETFL);       // make the reads non-blocking
 
//         memset(&tio,0,sizeof(tio));
//         tio.c_iflag=0;
//         tio.c_oflag=0;
//         tio.c_cflag=CS8|CREAD|CLOCAL;           // 8n1, see termios.h for more information
//         tio.c_lflag=0;
//         tio.c_cc[VMIN]=1;
//         tio.c_cc[VTIME]=0;
  
//         tty_fd=open(porta, O_RDWR );      
//         int out_s= cfsetospeed(&tio,B115200);           
//         int in_s= cfsetispeed(&tio,B115200);          
        
//         tcsetattr(tty_fd,TCSANOW,&tio);
// }

// int write_Servo(int a, int b, int c){        
        
//         char d[30];
//         memset(d, 0, sizeof(d));
//         char ca[30], cb[30], cc[30];
//         sprintf(ca, "%d", a);
//         sprintf(cb, "%d", b);
//         sprintf(cc, "%d", c);
//         strcpy(d, ",");
//         strcat(d, ca);
//         strcat(d, ",");
//         strcat(d, cb);
//         strcat(d, ",");
//         strcat(d, cc);
//         strcat(d,",\r\n");
//         write(tty_fd,d,strlen(d));
//         cout << d << endl;     
// }


//namespace controller_interface
//{

	// get URDF and name of root and tip from the parameter server
        //std::string robot_description, root_name, tip_name;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "lemon_delta_Node");

    ros::NodeHandle n;
    std::string robot_description;

    // init_Arduino();
    // sleep(5);

	   if (!ros::param::search(n.getNamespace(),"robot_description", robot_description))
        {
            ROS_ERROR_STREAM("KinematicChainControllerBase: No robot description (URDF) found on parameter server ("<<n.getNamespace()<<"/robot_description)");
            return false;
        }



        std::string xml_string;

        if (n.hasParam(robot_description))
            n.getParam(robot_description.c_str(), xml_string);
        else
        {
            ROS_ERROR("Parameter %s not set, shutting down node...", robot_description.c_str());
            n.shutdown();
            return false;
        }

        if (xml_string.size() == 0)
        {
            ROS_ERROR("Unable to load robot model from parameter %s",robot_description.c_str());
            n.shutdown();
            return false;
        }

        ROS_DEBUG("%s content\n%s", robot_description.c_str(), xml_string.c_str());


        urdf::Model model;
        if (!model.initString(xml_string))
        {
            ROS_ERROR("Failed to parse urdf file");
            n.shutdown();
            return false;
        }
        ROS_INFO("Successfully parsed urdf file");



        KDL::Tree kdl_tree_;


        if (!kdl_parser::treeFromUrdfModel(model, kdl_tree_))
        {
            ROS_ERROR("Failed to construct kdl tree");
            n.shutdown();
            return false;
        }

        
        KDL::Chain kdl_chain_a, kdl_chain_b,kdl_chain_c ;



        

    // Populate the KDL chain a
        if(!kdl_tree_.getChain("delta_box", "A_sixth_link", kdl_chain_a))
        {
            ROS_ERROR_STREAM("Failed to get KDL chain1 from tree: ");
            ROS_ERROR_STREAM("  "<<"delta_box"<<" --> "<<"A_sixth_link");
            ROS_ERROR_STREAM("  Tree has "<<kdl_tree_.getNrOfJoints()<<" joints");
            ROS_ERROR_STREAM("  Tree has "<<kdl_tree_.getNrOfSegments()<<" segments");
            ROS_ERROR_STREAM("  The segments are:");

            KDL::SegmentMap segment_map = kdl_tree_.getSegments();
            KDL::SegmentMap::iterator it;

            for( it=segment_map.begin(); it != segment_map.end(); it++ )
              ROS_ERROR_STREAM( "    "<<(*it).first);

            return false;
        }

    // Populate the KDL chain b
        if(!kdl_tree_.getChain("delta_box", "B_sixth_link", kdl_chain_b))
        {
            ROS_ERROR_STREAM("Failed to get KDL chain1 from tree: ");
            ROS_ERROR_STREAM("  "<<"delta_box"<<" --> "<<"B_sixth_link");
            ROS_ERROR_STREAM("  Tree has "<<kdl_tree_.getNrOfJoints()<<" joints");
            ROS_ERROR_STREAM("  Tree has "<<kdl_tree_.getNrOfSegments()<<" segments");
            ROS_ERROR_STREAM("  The segments are:");

            KDL::SegmentMap segment_map = kdl_tree_.getSegments();
            KDL::SegmentMap::iterator it;

            for( it=segment_map.begin(); it != segment_map.end(); it++ )
              ROS_ERROR_STREAM( "    "<<(*it).first);

            return false;
        }

    // Populate the KDL chain c
        if(!kdl_tree_.getChain("delta_box", "C_sixth_link", kdl_chain_c))
        {
            ROS_ERROR_STREAM("Failed to get KDL chain1 from tree: ");
            ROS_ERROR_STREAM("  "<<"delta_box"<<" --> "<<"C_sixth_link");
            ROS_ERROR_STREAM("  Tree has "<<kdl_tree_.getNrOfJoints()<<" joints");
            ROS_ERROR_STREAM("  Tree has "<<kdl_tree_.getNrOfSegments()<<" segments");
            ROS_ERROR_STREAM("  The segments are:");

            KDL::SegmentMap segment_map = kdl_tree_.getSegments();
            KDL::SegmentMap::iterator it;

            for( it=segment_map.begin(); it != segment_map.end(); it++ )
              ROS_ERROR_STREAM( "    "<<(*it).first);

            return false;
        }



    //while(ros::ok)
    {
    //namespace KDL {

        // Create solver based on kinematic chain
        KDL::ChainFkSolverPos_recursive fksolver1(kdl_chain_a);
        KDL::ChainFkSolverPos_recursive fksolver2(kdl_chain_b);
        KDL::ChainFkSolverPos_recursive fksolver3(kdl_chain_c);

        KDL::ChainIkSolverVel_pinv iksolver1v(kdl_chain_a);
        KDL::ChainIkSolverVel_pinv iksolver2v(kdl_chain_b);
        KDL::ChainIkSolverVel_pinv iksolver3v(kdl_chain_c);

        KDL::ChainIkSolverPos_NR iksolver1(kdl_chain_a,fksolver1,iksolver1v,100,1e-6);
        KDL::ChainIkSolverPos_NR iksolver2(kdl_chain_b,fksolver2,iksolver2v,100,1e-6);
        KDL::ChainIkSolverPos_NR iksolver3(kdl_chain_c,fksolver3,iksolver3v,100,1e-6);
        //Maximum 100 iterations, stop at accuracy 1e-6

        ROS_INFO("%d", kdl_chain_a.getNrOfJoints());
        ROS_INFO("%d", kdl_chain_b.getNrOfJoints());
        ROS_INFO("%d", kdl_chain_c.getNrOfJoints());

        KDL::JntArray q_init1(kdl_chain_a.getNrOfJoints());
        KDL::JntArray q_out1(kdl_chain_a.getNrOfJoints());

        KDL::JntArray q_init2(kdl_chain_b.getNrOfJoints());
        KDL::JntArray q_out2(kdl_chain_b.getNrOfJoints());

        KDL::JntArray q_init3(kdl_chain_c.getNrOfJoints());
        KDL::JntArray q_out3(kdl_chain_c.getNrOfJoints());

        // Create the frame that will contain the results
        // KDL::Frame cartpos1 ,cartpos2 ,cartpos3;

        // KDL::Rotation Rotation1(0.0,0.0,0.0);
        // KDL::Rotation Rotation2(0.0,0.0,-120*3.141592653589/180);
        // KDL::Rotation Rotation3(0.0,0.0,-240*3.141592653589/180);

        KDL::Vector Vector1(0.3748,0.0,0.20);
        KDL::Vector Vector2(0.3748,0.0,0.20);
        KDL::Vector Vector3(0.3748,0.0,0.20);

        KDL::Rotation Rotation1 = KDL::Rotation::RotX(0.0)*KDL::Rotation::RotY(0.0)*KDL::Rotation::RotZ(0.0);
        KDL::Rotation Rotation2 = KDL::Rotation::RotX(0.0)*KDL::Rotation::RotY(0.0)*KDL::Rotation::RotZ(-120*3.141592653589/180);
        KDL::Rotation Rotation3 = KDL::Rotation::RotX(0.0)*KDL::Rotation::RotY(0.0)*KDL::Rotation::RotZ(-240*3.141592653589/180);

        // Rotation1 = KDL::Rotation (0.0,0.0,0.0);
        // KDL::Vector Vector1 (0.3748,0.0,0.20);
        // Rotation2 = KDL::Rotation (0.0,0.0,-120*3.141592653589/180);
        // Vector2 = KDL::Vector (0.3748,0.0,0.20);
        // Rotation3 = (0.0,0.0,-240*3.141592653589/180);
        // Vector3 = KDL::Vector (0.3748,0.0,0.20);

        KDL::Frame cartpos1 (Rotation1, Vector1);
        KDL::Frame cartpos2 (Rotation2, Vector2);
        KDL::Frame cartpos3 (Rotation3, Vector3);

        //cartpos1 (Rotation1, Vector1);
        // cartpos2 (Rotation2, Vector2);
        // cartpos3 (Rotation3, Vector3);

        // Calculate inverse position kinematics
        // bool kinematics_status = iksolver.CartToJnt(jointpositions, cartpos);

        q_init1(0,0) =0;
        q_init1(1,0) =0;
        q_init1(2,0) =0;
        q_init1(3,0) =0;
        q_init1(4,0) =0;

        if (iksolver1.CartToJnt(q_init1, cartpos1, q_out1) < 0)
            ROS_INFO("solver for IK chain_a failed");
        if (iksolver2.CartToJnt(q_init2, cartpos2, q_out2) < 0)
            ROS_INFO("solver for IK chain_b failed");
        if (iksolver3.CartToJnt(q_init3, cartpos3, q_out3) < 0)
            ROS_INFO("solver for IK chain_c failed");

       // write_Servo(,,);
       // sleep(1);

    }

      /*  bool active_;

        int CartToJnt(const KDL::JntArray& q_init,
                const KDL::Frame& p_in,
                KDL::JntArray& q_out);




        q_init(1,0) = 0.0;
        q_init(1,1) = 0.0;
        q_init(1,2) = 0.0;

        //ik solver for the first chain
        if (iksolver1.CartToJnt(q_init, cartpos, q_out) < 0) {
            ROS_ERROR( "Something really bad happened. You are in trouble now");
            return -1;
        } else {
        // parse output of iksolver1 to the robot

            for (unsigned int j = 0; j < q_out.rows(); j++) {
                std::cout << q_out(1, j) * 180 / 3.14 << "  ";
            }
            std::cout << std::endl;
        }


        //ik solver for the second chain
        if (iksolver2.CartToJnt(q_init, cartpos, q_out) < 0) {
            ROS_ERROR( "Something really bad happened. You are in trouble now");
            return -1;
        } else {
        // parse output of iksolver2 to the robot

            for (unsigned int j = 0; j < q_out.rows(); j++) {
                std::cout << q_out(1, j) * 180 / 3.14 << "  ";
            }
            std::cout << std::endl;
        }

        //ik solver for the third chain
        if (iksolver3.CartToJnt(q_init, cartpos, q_out) < 0) {
            ROS_ERROR( "Something really bad happened. You are in trouble now");
            return -1;
        } else {
        // parse output of iksolver3 to the robot

            for (unsigned int j = 0; j < q_out.rows(); j++) {
                std::cout << q_out(1, j) * 180 / 3.14 << "  ";
            }
            std::cout << std::endl;
        }
*/


}