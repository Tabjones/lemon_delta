#ifndef DELTA_IK___DELTA_IK_H
#define DELTA_IK___DELTA_IK_H

#include <cmath>
#include <iostream>

/*
 * Constants
 * ToDo: put it in a header and include it
 *
*/
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
const double sin120 = sin(120*M_PI/180);
const double cos120 = cos(120*M_PI/180);
const double eps = 0.000000001;

namespace delta_kinematics
{

/*
 * \brief Container for delta parameters.
 * Letters use:
 * - b for base
 * - p for platform
 * - l for length of base-attached and platform attached legs
 *
 * Delta construction assumptions:
 * 1. One point on platform and base, the other two are obtained by rotating the
 *    point by 120deg in their corresponding frames.
 * 2. All legs lengths, those attached to the platform and base, are equal,
 *    correspondingly.
 * 3. Z-axis of base and platform points up (0,0,1), for rotations. X- and
 *    Y-axes are the same for base and platfor, so witout loss of generality
 *    Rb = Rp = I
 *
 * ToDo: The params should be done in a DeltaRobot class, so the solver
 * could be initialized with a DeltaRobot object.
 */
class DeltaLeg
{
public:
    // joint vectors in base coordinates, ensure they are unit vectors
    // a = revolute axis
    // u0 = ref. vector when angle = 0
    // note that a and u0 define the joint reference frame
    double a_x;
    double a_y;
    double a_z;
    double u0_x;
    double u0_y;
    double u0_z;

    // attaching point in base, in base coordinate system
    // note: should be the intersection of lines oriented with a and u0 above
    double b_x;
    double b_y;
    double b_z;

    // attaching point in platform, in platform coordinates
    double p_x;
    double p_y;
    double p_z;

    // leg lengths
    // b -> proximal leg
    // p -> distal leg
    double l_b;
    double l_p;

    // controls leg position and limits in rads
    double angle;
    double angle_min;
    double angle_max;
};
class DeltaRobot
{
public:
    // leg 1 -> +0deg
    // leg 2 -> +120deg
    // leg 3 -> -120deg
    DeltaLeg leg1;
    DeltaLeg leg2;
    DeltaLeg leg3;
};

/*
 * Helper functions to build a delta, recall Delta construction assumptions above
 */
void createDeltaRobot(const DeltaLeg &leg, DeltaRobot &delta);
void createDeltaLeg(double &a_x, double &a_y, double &a_z,
                    double &u0_x, double &u0_y, double &u0_z,
                    double &b_x, double &b_y, double &b_z, double &l_b,
                    double &p_x, double &p_y, double &p_z, double &l_p, DeltaLeg &leg);
void rotateLeg120Right(const DeltaLeg &legin, DeltaLeg &legout);
void rotateLeg120Left(const DeltaLeg &legin, DeltaLeg &legout);
void rotate120Left(const double &x_in, const double &y_in, const double &z_in,
                double &x_out, double &y_out, double &z_out);
void rotate120Right(const double &x_in, const double &y_in, const double &z_in,
                double &x_out, double &y_out, double &z_out);
double computeMinAngle(double vref_x, double vref_y, double vref_z,
                     double vx, double vy, double vz);
bool isWithin(const double x, const double x_min, const double x_max);
bool isNormalized(double vx, double vy, double vz);

/*
 * DeltaIKSolver class
 *
 */
class DeltaIKSolver
{
public:

    virtual ~DeltaIKSolver() {}
    DeltaIKSolver();
    DeltaIKSolver(DeltaRobot &delta);
    DeltaIKSolver(DeltaLeg &leg);

    /**
     * \brief Computes the motor angles given the centroid point of the platform
     * \param[in]  Desired 3D point.
     * \param[out] Found joint angles
     * \return Error code: true solution found
     *                         false solution not found
     */
    bool CartToJnt(const double &x, const double &y, const double &z,
            double &ang1, double &ang2, double &ang3);

private:

    // internal delta robot
    DeltaRobot delta_;

    // set current leg parameters
    void setCurrentLeg(DeltaLeg &leg);
    // current leg parameters to ease computation
    double a_x_, a_y_, a_z_;
    double u0_x_, u0_y_, u0_z_;
    double b_x_, b_y_, b_z_;
    double p_x_, p_y_, p_z_;
    double l_b_, l_p_;
    double angle_max_, angle_min_;

    // set current target
    void setCurrentGoal(const double x, const double y, const double z);
    // current target
    double x_, y_, z_;

    // called after leg and goal are set
    bool solveCurrentLeg(double &angle);

    // generic solver function
    bool CartToJnt(const double &x, const double &y, const double &z,
                    DeltaRobot &myDelta);


};

}

#endif
