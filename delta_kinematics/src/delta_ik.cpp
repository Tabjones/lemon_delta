#include <delta_kinematics/delta_ik.h>

namespace delta_kinematics
{
    /*
     ******************
     * HELPER methods *
     ******************
     */

    void createDeltaRobot(const DeltaLeg &leg, DeltaRobot &delta)
    {
        if(!isNormalized(leg.a_x, leg.a_y, leg.a_z))
            std::cout << "Input revolute axis vector is not normalized." << std::endl << "Please, ensure it is a unit vector" << std::endl;
        if(!isNormalized(leg.u0_x, leg.u0_y, leg.u0_z))
            std::cout << "Input vector for rotation reference is not normalized." << std::endl << "Please, ensure it is a unit vector" << std::endl;

        delta.leg1 = leg;
        delta.leg2 = leg;
        delta.leg3 = leg;
        rotateLeg120Left(leg, delta.leg3);
        rotateLeg120Right(leg, delta.leg2);

        // std::cout << "Delta Robot created with parameters:" << std::endl;
        // std::cout << "a1: (" << delta.leg1.a_x << ", " << delta.leg1.a_y << ", " << delta.leg1.a_z << ")" << std::endl;
        // std::cout << "a2: (" << delta.leg2.a_x << ", " << delta.leg2.a_y << ", " << delta.leg2.a_z << ")" << std::endl;
        // std::cout << "a3: (" << delta.leg3.a_x << ", " << delta.leg3.a_y << ", " << delta.leg3.a_z << ")" << std::endl;
        // std::cout << "u01: (" << delta.leg1.u0_x << ", " << delta.leg1.u0_y << ", " << delta.leg1.u0_z << ")" << std::endl;
        // std::cout << "u02: (" << delta.leg2.u0_x << ", " << delta.leg2.u0_y << ", " << delta.leg2.u0_z << ")" << std::endl;
        // std::cout << "u03: (" << delta.leg3.u0_x << ", " << delta.leg3.u0_y << ", " << delta.leg3.u0_z << ")" << std::endl;
        // std::cout << "b1: (" << delta.leg1.b_x << ", " << delta.leg1.b_y << ", " << delta.leg1.b_z << ")" << std::endl;
        // std::cout << "b2: (" << delta.leg2.b_x << ", " << delta.leg2.b_y << ", " << delta.leg2.b_z << ")" << std::endl;
        // std::cout << "b3: (" << delta.leg3.b_x << ", " << delta.leg3.b_y << ", " << delta.leg3.b_z << ")" << std::endl;
        // std::cout << "p1: (" << delta.leg1.p_x << ", " << delta.leg1.p_y << ", " << delta.leg1.p_z << ")" << std::endl;
        // std::cout << "p2: (" << delta.leg2.p_x << ", " << delta.leg2.p_y << ", " << delta.leg2.p_z << ")" << std::endl;
        // std::cout << "p3: (" << delta.leg3.p_x << ", " << delta.leg3.p_y << ", " << delta.leg3.p_z << ")" << std::endl;
        // std::cout << "l1_b: " << delta.leg1.l_b << std::endl;
        // std::cout << "l1_p: " << delta.leg1.l_p << std::endl;
        // std::cout << "l2_b: " << delta.leg2.l_b << std::endl;
        // std::cout << "l2_p: " << delta.leg2.l_p << std::endl;
        // std::cout << "l3_b: " << delta.leg3.l_b << std::endl;
        // std::cout << "l3_p: " << delta.leg3.l_p << std::endl;
        // std::cout << "angle1 limits: [" << delta.leg1.angle_min << ", " << delta.leg1.angle_max << "]" << std::endl;
        // std::cout << "angle2 limits: [" << delta.leg2.angle_min << ", " << delta.leg2.angle_max << "]" << std::endl;
        // std::cout << "angle3 limits: [" << delta.leg3.angle_min << ", " << delta.leg3.angle_max << "]" << std::endl;
    }

    void createDeltaLeg(double &a_x, double &a_y, double &a_z,
                        double &u0_x, double &u0_y, double &u0_z,
                        double &b_x, double &b_y, double &b_z, double &l_b,
                        double &p_x, double &p_y, double &p_z, double &l_p,
                        DeltaLeg &leg)
    {
        leg.a_x = a_x;
        leg.a_y = a_y;
        leg.a_z = a_z;
        leg.u0_x = u0_x;
        leg.u0_y = u0_y;
        leg.u0_z = u0_z;
        leg.b_x = b_x;
        leg.b_y = b_y;
        leg.b_z = b_z;
        leg.l_b = l_b;
        leg.p_x = p_x;
        leg.p_y = p_y;
        leg.p_z = p_z;
        leg.l_p = l_p;
    }

    void rotateLeg120Right(const DeltaLeg &legin, DeltaLeg &legout)
    {
        rotate120Right(legin.a_x, legin.a_y, legin.a_z,
                      legout.a_x, legout.a_y, legout.a_z);
        rotate120Right(legin.u0_x, legin.u0_y, legin.u0_z,
                      legout.u0_x, legout.u0_y, legout.u0_z);
        rotate120Right(legin.b_x, legin.b_y, legin.b_z,
                      legout.b_x, legout.b_y, legout.b_z);
        rotate120Right(legin.p_x, legin.p_y, legin.p_z,
                      legout.p_x, legout.p_y, legout.p_z);
    }

    void rotateLeg120Left(const DeltaLeg &legin, DeltaLeg &legout)
    {
        rotate120Left(legin.a_x, legin.a_y, legin.a_z,
                      legout.a_x, legout.a_y, legout.a_z);
        rotate120Left(legin.u0_x, legin.u0_y, legin.u0_z,
                      legout.u0_x, legout.u0_y, legout.u0_z);
        rotate120Left(legin.b_x, legin.b_y, legin.b_z,
                      legout.b_x, legout.b_y, legout.b_z);
        rotate120Left(legin.p_x, legin.p_y, legin.p_z,
                      legout.p_x, legout.p_y, legout.p_z);
    }

    void rotate120Right(const double &x_in, const double &y_in, const double &z_in,
                    double &x_out, double &y_out, double &z_out)
    {
        x_out = cos120*x_in - sin120*y_in;
        y_out = sin120*x_in + cos120*y_in;
        z_out = z_in;
    }

    void rotate120Left(const double &x_in, const double &y_in, const double &z_in,
                    double &x_out, double &y_out, double &z_out)
    {
        x_out =  cos120*x_in + sin120*y_in;
        y_out = -sin120*x_in + cos120*y_in;
        z_out = z_in;
    }

    double computeMinAngle(double vref_x, double vref_y, double vref_z,
                           double vx, double vy, double vz)
    {
        // ToDo: Use atan2(Y, X), where Y is computed using the cross product
        double X = vref_x*vx + vref_y*vy + vref_z*vz;
        return acos(X);
    }

    bool isWithin(const double x, const double x_min, const double x_max)
    {
        if( x > x_max || x < x_min )
            return false;
        return true;
    }

    bool isNormalized(double vx, double vy, double vz)
    {
        if( vx*vx + vy*vy + vz*vz - 1.0 < eps )
            return true;
        else
            return false;
    }

    /*
     ************************
     * CONSTRUCTORS methods *
     ************************
     */

    DeltaIKSolver::DeltaIKSolver(){}

    DeltaIKSolver::DeltaIKSolver(DeltaRobot &delta)
    {
        delta_ = delta;
    }

    DeltaIKSolver::DeltaIKSolver(DeltaLeg &leg)
    {
        createDeltaRobot(leg, delta_);
    }

    /*
     ******************
     * PUBLIC methods *
     ******************
     */

    bool DeltaIKSolver::CartToJnt(const double &x, const double &y, const double &z,
                double &ang1, double &ang2, double &ang3)
    {
        if (DeltaIKSolver::CartToJnt(x, y, z, delta_)){
            ang1=delta_.leg1.angle;
            ang2=delta_.leg2.angle;
            ang3=delta_.leg3.angle;
            return (true);
        }
        ang1 = ang2 = ang3 = 0.0;
        return (false);
    }

    /*
     *******************
     * PRIVATE methods *
     *******************
     */

    void DeltaIKSolver::setCurrentLeg(DeltaLeg &leg)
    {
        a_x_ = leg.a_x;
        a_y_ = leg.a_y;
        a_z_ = leg.a_z;
        u0_x_ = leg.u0_x;
        u0_y_ = leg.u0_y;
        u0_z_ = leg.u0_z;
        b_x_ = leg.b_x;
        b_y_ = leg.b_y;
        b_z_ = leg.b_z;
        l_b_ = leg.l_b;
        p_x_ = leg.p_x;
        p_y_ = leg.p_y;
        p_z_ = leg.p_z;
        l_p_ = leg.l_p;
        angle_max_ = leg.angle_max;
        angle_min_ = leg.angle_min;
    }

    void DeltaIKSolver::setCurrentGoal(const double x, const double y, const double z)
    {
        x_ = x;
        y_ = y;
        z_ = z;
    }

    // Per-leg solution
    // ToDo: choose solution, right now it assumes that limits will allow only one solution
    bool DeltaIKSolver::solveCurrentLeg(double &angle)
    {
        angle = 0.0;

        // compute p in base frame, recall rotations are the identity
        double pp_x = x_ + p_x_;
        double pp_y = y_ + p_y_;
        double pp_z = z_ + p_z_;

        // per-leg feasibility check
        // ToDo: move to separate functions to test them
        // 1st check: plane(a,b)-sphere(pp,lp)
        double p = -a_x_*b_x_ - a_y_*b_y_ - a_z_*b_z_;
        double np = a_x_*pp_x + a_y_*pp_y + a_z_*pp_z;
        if( abs(np + p) > l_p_ )
        {
            std::cout << "Failed feasibility of: plane(a,b)-sphere(pp,lp) in leg: " << std::endl;
            return false;
        }

        // 2nd check: sphere(b,lb)-sphere(pp,lp)
        // ||b - pp|| < l_b_ + l_p_
        double q;
        q = pp_x*pp_x - 2*pp_x*b_x_ + b_x_*b_x_;
        q += pp_y*pp_y - 2*pp_y*b_y_ + b_y_*b_y_;
        q += pp_z*pp_z - 2*pp_z*b_z_ + b_z_*b_z_;
        q = sqrt( q );
        if( q > (l_b_ + l_p_) )
        {
            std::cout << "Failed feasibility of: sphere(b,lb)-sphere(pp,lp) in leg: " << std::endl;
            return false;
        }

        // create 1-subs-dependant constants
        double ppbx = (b_x_ - pp_x);
        double ppby = (b_y_ - pp_y);
        double ppbz = (b_z_ - pp_z);
        double cx = +2*l_b_*ppbx;
        double cy = +2*l_b_*ppby;
        double cz = +2*l_b_*ppbz;
        double c = ppbx*ppbx + ppby*ppby + ppbz*ppbz + l_b_*l_b_ - l_p_*l_p_;

        // create 2-subs-dependant constants
        double d_den = cy/cx - a_y_/a_x_;
        d_den = 1/d_den;
        double dz = -1*(cz/cx - a_z_/a_x_)*d_den;
        double d = -1*(c/cx)*d_den;
        double ez = -(a_y_*dz + a_z_)/a_x_;
        double e = -a_y_*d/a_x_;

        // create quadratic polynomial constants
        double A = ez*ez + dz*dz + 1.0;
        double B = 2*ez*e + 2*dz*d;
        double C = e*e + d*d - 1.0;

        // if feasibility check passed, then shouldn't be a problem
        // but sanity check of: discriminant(B*B - 4*A*C)
        double D = B*B - 4*A*C;
        // std::cout << "A: " << A << std::endl << std::flush;
        // std::cout << "B: " << B << std::endl << std::flush;
        // std::cout << "C: " << C << std::endl << std::flush;
        // std::cout << "D: " << D << std::endl << std::flush;
        if( D < 0.0 || std::isnan(D) )
        {
            std::cout << "Failed double check feasibility of: discriminant(B*B - 4*A*C) in leg: " << std::endl;
            return false;
        }

        // two solutions to choose from...
        double uz1 = (-B + sqrt(D))/(2*A);
        double uz2 = (-B - sqrt(D))/(2*A);

        // backsubstitution
        double ux1 = ez*uz1 + e;
        double uy1 = dz*uz1 + d;

        double ux2 = ez*uz2 + e;
        double uy2 = dz*uz2 + d;

        // std::cout << "Solution 1: (" << ux1 << ", " << uy1 << ", " << uz1 << ")" << std::endl;
        // std::cout << "Solution 2: (" << ux2 << ", " << uy2 << ", " << uz2 << ")" << std::endl;

        angle = computeMinAngle(u0_x_, u0_y_, u0_z_, ux1, uy1, uz1);
        if( isWithin(angle, angle_min_, angle_max_))
            return true;

        angle = computeMinAngle(u0_x_, u0_y_, u0_z_, ux2, uy2, uz2);
        if( isWithin(angle, angle_min_, angle_max_))
            return true;

        std::cout << "Could not find an angle within limits in leg: " << std::endl;
        return false;
    }

    bool DeltaIKSolver::CartToJnt(const double &x, const double &y, const double &z,
                                    DeltaRobot &myDelta)
    {
        myDelta.leg1.angle = 0.0;
        myDelta.leg2.angle = 0.0;
        myDelta.leg3.angle = 0.0;

        setCurrentGoal(x, y, z);

        setCurrentLeg(myDelta.leg1);
        if( !solveCurrentLeg(myDelta.leg1.angle))
            return false;

        setCurrentLeg(myDelta.leg2);
        if( !solveCurrentLeg(myDelta.leg2.angle))
            return false;

        setCurrentLeg(myDelta.leg3);
        if( !solveCurrentLeg(myDelta.leg3.angle))
            return false;

        std::cout << "Solution found: angle1= " << myDelta.leg1.angle <<
                                    " angle2= " << myDelta.leg2.angle <<
                                    " angle3= " << myDelta.leg3.angle << std::endl;
        return true;
    }
}
